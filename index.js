"use strict";

let pulumi = require("@pulumi/pulumi");
let aws = require("@pulumi/aws");
let awsx = require("@pulumi/awsx");
let eks = require("@pulumi/eks");
let kubernetes = require("@pulumi/kubernetes");
let gitlab = require("@pulumi/gitlab");

let project = new gitlab.Project("knative-ruby-app", {
  name: "knative-ruby-app",
  defaultBranch: "master",
  requestAccessEnabled: false,
  visibilityLevel: "public",
}, { import: "mattkasa/knative-ruby-app"});

let cluster = new eks.Cluster("mkasa-eks-1");

exports.kubeconfig = cluster.kubeconfig;

let clusterNamespace = new kubernetes.core.v1.Namespace("knative-ruby-app-production", {
  metadata: {
    name: pulumi.interpolate `${project.name}-${project.id}-production`,
  },
}, { provider: cluster.provider });

let serviceAccount = new kubernetes.core.v1.ServiceAccount("gitlab", {
  metadata: {
    name: "gitlab",
    namespace: "kube-system",
  },
}, { provider: cluster.provider });

let clusterRoleBinding = new kubernetes.rbac.v1.ClusterRoleBinding("gitlab-admin", {
  metadata: {
    name: "gitlab-admin",
  },
  subjects: [{
     kind: "ServiceAccount",
     name: serviceAccount.metadata.name,
     namespace: serviceAccount.metadata.namespace,
  }],
  roleRef: {
    kind: "ClusterRole",
    name: "cluster-admin",
    apiGroup: "rbac.authorization.k8s.io",
  },
}, { provider: cluster.provider });

let secret = new kubernetes.core.v1.Secret("gitlab-admin", {
  type: "kubernetes.io/service-account-token",
  metadata: {
    name: "gitlab-admin",
    annotations: {
      "kubernetes.io/service-account.name": serviceAccount.metadata.name,
    },
  },
}, { provider: cluster.provider });
